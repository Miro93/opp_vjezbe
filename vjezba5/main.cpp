#include <iostream>
#include <vector>
#include "ZooAnimal.h"
using namespace std;


void promjena_tezine(ZooAnimal& animal)
{
	int n;
	n = animal.promjena_tezine();
	if (n == 1)
	{
		animal.promjena_obroka('-');
	}
	else if (n == -1)
	{
		animal.promjena_obroka('+');
	}
	else if (n == 0)
	{
		animal.promjena_obroka('0');
	}
}

int main()
{

	int n, m, a = 1, b;
	string vrsta, ime;
	int br_obroka, vijek, g_rodenja, br_kaveza;
	vector<ZooAnimal> zivotinje;
	cout << "Unesi broj zivotinja" << endl;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Vrsta:" << endl;
		cin >> vrsta;
		cout << "Ime:" << endl;
		cin >> ime;
		cout << "Godina rodenja:" << endl;
		cin >> g_rodenja;
		cout << "Broj kaveza:" << endl;
		cin >> br_kaveza;
		cout << "Broj obroka:" << endl;
		cin >> br_obroka;
		cout << "Ocekivani zivotni vijek:" << endl;
		cin >> vijek;
		zivotinje.emplace_back(vrsta, ime, g_rodenja, br_kaveza, br_obroka, vijek);
		cout << "Za kraj unosa unesite 0" << endl;
		while (a != 0)
		{
			{
				cout << "Godina:" << endl;
				cin >> a;
				if (a == 0)
					break;
				cout << "Kilogrami" << endl;
				cin >> b;
				zivotinje[i].dodavanje_mase(b, a);
			}
		}
		promjena_tezine(zivotinje[i]);
		zivotinje[i].ispisivanje();
		a = 1;
	}
}
