#pragma once
#include <string>
#include<vector>
using namespace std;

class ZooAnimal
{
public:
	ZooAnimal(const string v, const string i, const int r_g, const int br_k, const int br_ob, const int vj);
	string vrsta;
	string ime;
	int rodenje_g, br_kaveza, br_obroka, vijek;
	int* masa;
	int* godina;
	ZooAnimal(const ZooAnimal& animal);
	void promjena_obroka(const char& c);
	void ispisivanje();
	void dodavanje_mase(const float& m, const int& g);
	int promjena_tezine();
	~ZooAnimal();
};

