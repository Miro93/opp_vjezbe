#include "ZooAnimal.h"
#include <ctime>
#include <iostream>
#include <ctime>
using namespace std;


ZooAnimal::ZooAnimal(const string v, const string i, const int r_g, const int br_k, const int br_ob, const int vj)
{
	cout << "Konstruktor\n";
	vrsta = v;
	ime = i;
	rodenje_g = r_g;
	br_kaveza = br_k;
	br_obroka = br_ob;
	vijek = vj;
	masa = new int[vijek * 2];
	godina = new int[vijek * 2];
	for (int i = 0; i < vijek * 2; i++)
	{
		masa[i] = 0;
		godina[i] = 0;
	}
}
ZooAnimal::ZooAnimal(const ZooAnimal& animal)
{
	cout << "Copy constructor \n";
	vrsta = animal.vrsta;
	ime = animal.ime;
	rodenje_g = animal.rodenje_g;
	br_kaveza = animal.br_kaveza;
	br_obroka = animal.br_obroka;
	vijek = animal.vijek;
	masa = new int[vijek * 2];
	godina = new int[vijek * 2];
	for (int i = 0; i < vijek * 2; i++)
	{
		masa[i] = animal.masa[i];
		godina[i] = animal.godina[i];
	}

}
void ZooAnimal::promjena_obroka(const char& c)
{
	if (c == '-')
	{
		br_obroka--;
		std::cout << "Broj obroka smanjen na " << br_obroka << "\n";
	}
	else if (c == '+')
	{
		br_obroka++;
		std::cout << "Broj obroka povecan na " << br_obroka << "\n";
	}
	else if (c == '0')
	{
		cout << "Broj obroka ostaje isti\n";
	}
	else
		cout << "Nepoznat unos\n";
}
void ZooAnimal::ispisivanje()
{
	cout << "Ime: " << ime << endl;
	cout << "Vrsta: " << vrsta << endl;
	cout << "Okocena: " << rodenje_g << endl;
	cout << "Kavez broj: " << br_kaveza << endl;
	cout << "Ocekivani zivotni vijek: " << vijek << endl;
	cout << "Broj obroka dnevno: " << br_obroka << endl;
	cout << "Godina: " << endl;
	for (int i = 0; i < vijek * 2; i++)
	{
		if (godina[i] != 0)
		{
			cout << godina[i] << " ";
		}
	}
	cout << endl;
	cout << "Tezina: " << endl;
	for (int i = 0; i < vijek * 2; i++)
	{
		if (masa[i] != 0)
		{
			cout << masa[i] << " ";
		}
	}
	cout << endl;
}
void ZooAnimal::dodavanje_mase(const float& m, const int& g)
{
	time_t now = time(0);
	tm *local_time = localtime(&now);
	time_t current_Year = 1900 + local_time->tm_year;
	if (g > current_Year || g < rodenje_g)
	{
		cout << "Nemoguce upisati podatke za tu godinu" << endl;
	}
	else
	{
		for (int i = 0; i < vijek * 2; i++)
		{
			if (godina[i] == g)
			{
				cout << "Podaci za tu godinu su vec upisani" << endl;
				break;
			}
			else if (godina[i] == 0)
			{
				godina[i] = g;
				masa[i] = m;
				break;
			}

		}
	}
}
int ZooAnimal::promjena_tezine()
{
	float masa_trenutna = 0, masa_prosla = 0;
	int flag_1 = 0, flag_2 = 0;
	time_t now = time(0);
	tm *local_time = localtime(&now);
	time_t current_Year = 1900 + local_time->tm_year;
	for (int i = 0; i < vijek * 2; i++)
	{
		if (godina[i] == current_Year)
		{
			flag_1 = 1;
			masa_trenutna = masa[i];
		}
		if (godina[i] == (current_Year - 1))
		{
			flag_2 = 1;
			masa_prosla = masa[i];
		}
	}
	if ((flag_1 = 1) && (flag_2 = 1))
	{
		if (masa_trenutna > (masa_prosla + masa_prosla * 0.1))
		{
			cout << "Pretila" << endl;
			return 1;
		}
		if (masa_prosla > (masa_trenutna + masa_trenutna * 0.1))
		{
			cout << "Pothranjena" << endl;
			return -1;
		}
		else
		{
			cout << "OK" << endl;
			return 0;
		}
	}
	else
		cout << "Nema podataka" << endl;

}

ZooAnimal::~ZooAnimal()
{
	delete[] masa;
	masa = NULL;
	delete[] godina;
	godina = NULL;
	cout << "Destructor\n";
}
