#pragma once
#include "Mammal.h"
using namespace oss;

class Monkey : public Mammal {
public:
	Monkey(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Monkey() : Mammal() {}
};