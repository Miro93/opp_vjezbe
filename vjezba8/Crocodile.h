#pragma once
#include "Reptile.h"
using namespace oss;

class Crocodile : public Reptile {
public:
	Crocodile(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Crocodile() : Reptile() {}
};