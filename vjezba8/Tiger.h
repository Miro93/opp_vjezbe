#pragma once
#include "Mammal.h"
using namespace oss;

class Tiger : public Mammal {
public:
	Tiger(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Tiger() : Mammal() {}
};

