#include "Turtle.h"
#include "Reptile.h"
using namespace oss;

Turtle::Turtle(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Reptile("Turtle", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	incubation_time = 2;
	ambient_temperature = 38.3;
	prescribed_amount_of_food = 15;
}

