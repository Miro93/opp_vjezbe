#include "ZooAnimal.h"
using namespace oss;
#pragma once

class Reptile : public ZooAnimal {
protected:
	string reproduction_way;
	int incubation_time;
	double ambient_temperature;
	int prescribed_amount_of_food;
public:
	Reptile(string aSpecies, string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
		: ZooAnimal(aSpecies, aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime), reproduction_way("laying eggs") {}
	Reptile() : ZooAnimal(), reproduction_way("giving birth"), incubation_time(0), ambient_temperature(0), prescribed_amount_of_food(0) {}
	void printAnimalData(ostream &output) const override;
	void inputAnimalData(istream &input) override;
};