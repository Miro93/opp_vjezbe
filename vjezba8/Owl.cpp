#include "Owl.h"
#include "Bird.h"
using namespace oss;

Owl::Owl(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Bird("Owl", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	incubation_time = 1;
	average_body_temperature = 32.3;
	prescribed_amount_of_food = 7;
}
