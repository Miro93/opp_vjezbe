#include "ZooAnimal.h"
#include <iostream>
#include <ctime>
#include "Bird.h"
#include "Mammal.h"
#include "Reptile.h"

namespace oss {
	ZooAnimal::ZooAnimal() {
		species = "";
		name = "";
		birthYear = 0;
		cageNumber = 0;
		mealsPerDay = 0;
		expectedLifeTime = 0;
		weightData = new float*[2];
		for (int i = 0; i < 2; i++) {
			weightData[i] = new float[2 * expectedLifeTime];
			for (int j = 0; j < 2 * expectedLifeTime; j++) {
				weightData[i][j] = 0;
			}
		}
	}

	ZooAnimal::ZooAnimal(const string aSpecies, const string aName, const int aBirthYear, const int aCageNumber, const unsigned short aMealsPerDay, const unsigned short aExpectedLifeTime) {
		species = aSpecies;
		name = aName;
		birthYear = aBirthYear;
		cageNumber = aCageNumber;
		mealsPerDay = aMealsPerDay;
		expectedLifeTime = aExpectedLifeTime;
		weightData = new float*[2];
		for (int i = 0; i < 2; i++) {
			weightData[i] = new float[2 * expectedLifeTime];
			for (int j = 0; j < 2 * expectedLifeTime; j++) {
				weightData[i][j] = 0;
			}
		}
		cout << "Animal created.\n";
	}

	ZooAnimal::ZooAnimal(const ZooAnimal& animalToCopy) {
		species = animalToCopy.species;
		name = animalToCopy.name;
		birthYear = animalToCopy.birthYear;
		cageNumber = animalToCopy.cageNumber;
		mealsPerDay = animalToCopy.mealsPerDay;
		expectedLifeTime = animalToCopy.expectedLifeTime;
		weightData = new float*[2];
		for (int i = 0; i < 2; i++) {
			weightData[i] = new float[2 * expectedLifeTime];
			for (int j = 0; j < 2 * expectedLifeTime; j++) {
				weightData[i][j] = animalToCopy.weightData[i][j];
			}
		}
		cout << "Animal copied.\n";
	}

	ZooAnimal::~ZooAnimal() {
		for (int i = 0; i < 2; i++) {
			delete[] weightData[i];
		}
		delete[] weightData;
		cout << "Animal deleted.\n";
	}

	int ZooAnimal::getMealsPerDay() {
		return mealsPerDay;
	}

	void ZooAnimal::changeMeals(int x) {
		x > 0 && x != 0 ? mealsPerDay++ : mealsPerDay--;
	}

	void ZooAnimal::addWeightData(int wYear, float wData) {
		time_t rawTime = time(NULL);
		char* currentTime = ctime(&rawTime);
		struct tm* myTime = localtime(&rawTime);
		int currentYear = 1900 + myTime->tm_year;
		if (wYear > currentYear || wYear < birthYear) {
			cout << "Invalid year entered.\n";
		}
		else if (wYear <= currentYear) {
			int i = 0;
			for (i; i < 2 * expectedLifeTime; i++) {
				if (wYear == weightData[0][i]) {
					if (wYear == currentYear) {
						weightData[1][i] = wData;
					}
					else {
						cout << "Data for this year already entered.\n";
					}
					break;
				}
			}
			if (i == 2 * expectedLifeTime) {
				for (int j = 0; j < 2 * expectedLifeTime; j++) {
					if (weightData[0][j] == 0) {
						weightData[0][j] = wYear;
						weightData[1][j] = wData;
						break;
					}
				}
			}
		}
	}

	int ZooAnimal::gainedOrLoosedLotOfWeight() const {
		time_t rawTime = time(NULL);
		char* currentTime = ctime(&rawTime);
		struct tm* myTime = localtime(&rawTime);
		int currentYear = 1900 + myTime->tm_year;
		int j = -1, k = -1;
		for (int i = 0; i < 2 * expectedLifeTime; i++) {
			if (weightData[0][i] == currentYear) {
				j = i;
			}
			else if (weightData[0][i] == currentYear - 1) {
				k = i;
			}
		}
		if (j != -1 && k != -1) {
			float percentage = 0;
			if (weightData[1][j] > weightData[1][k]) {
				percentage = 1 - weightData[1][k] / weightData[1][j];
			}
			else {
				percentage = -1 + weightData[1][j] / weightData[1][k];
			}
			if (percentage > 0.1) {
				return 1;
			}
			else if (percentage < -0.1) {
				return -1;
			}
			else {
				return 0;
			}
		}
		cout << "Not enough data to calculate!\n";
		return 0;
	}

	int ZooAnimal::counter = 0;

	void ZooAnimal::incrementCounter() {
		counter++;
	}

	void ZooAnimal::decrementCounter() {
		counter--;
	}

	bool ZooAnimal::operator==(const ZooAnimal & a) {
		return (this->species == a.species && this->name == a.name);
	}

	ZooAnimal ZooAnimal::operator++() {
		++mealsPerDay;
		return ZooAnimal(species, name, birthYear, cageNumber, mealsPerDay, expectedLifeTime);
	}

	ZooAnimal ZooAnimal::operator++(int) {
		mealsPerDay++;
		return ZooAnimal(species, name, birthYear, cageNumber, mealsPerDay, expectedLifeTime);
	}

	void ZooAnimal::operator=(const ZooAnimal & a) {
		species = a.species;
		name = a.name;
		birthYear = a.birthYear;
		cageNumber = a.cageNumber;
		mealsPerDay = a.mealsPerDay;
		expectedLifeTime = a.expectedLifeTime;
		weightData = new float*[2];
		for (int i = 0; i < 2; i++) {
			weightData[i] = new float[2 * expectedLifeTime];
			for (int j = 0; j < 2 * expectedLifeTime; j++) {
				weightData[i][j] = a.weightData[i][j];
			}
		}
	}

	ostream & operator<<(ostream & output, const ZooAnimal & a) {
		output << "\nSpecies: " << a.species << endl << "Name: " << a.name << endl
			<< "Birth year: " << a.birthYear << endl << "Cage number: " << a.cageNumber << endl
			<< "Meals per day: " << a.mealsPerDay << endl << "Expected lifetime: " << a.expectedLifeTime << endl;
		a.printAnimalData(output);
		/*
		output << "Weight data:\n" << "Year:\n";
		for (int i = 0; i < 2 * a.expectedLifeTime; i++) {
			output << a.weightData[0][i] << "\t";
		}
		output << "\nKg:\n";
		for (int i = 0; i < 2 * a.expectedLifeTime; i++) {
			output << a.weightData[1][i] << "\t";
		}
		*/
		output << endl;
		return output;
	}

	istream & operator>>(istream & input, ZooAnimal & a) {
		cout << "\nSpecies: ";
		input >> a.species;
		cout << "Name: ";
		input >> a.name;
		cout << "Birth year: ";
		input >> a.birthYear;
		cout << "Cage number: ";
		input >> a.cageNumber;
		cout << "Meals per day: ";
		input >> a.mealsPerDay;
		a.inputAnimalData(input);
		cout << "Expected lifetime: ";
		input >> a.expectedLifeTime;
		a.weightData = new float*[2];
		for (int i = 0; i < 2; i++) {
			a.weightData[i] = new float[2 * a.expectedLifeTime];
			for (int j = 0; j < 2 * a.expectedLifeTime; j++) {
				a.weightData[i][j] = 0;
			}
		}
		cout << "How many years of weight data you want to enter: ";
		int m;
		input >> m;
		for (int j = 0; j < m; j++) {
			int x;
			float y;
			cout << "Year: ";
			input >> x;
			cout << "Weight (in kg): ";
			input >> y;
			a.addWeightData(x, y);
		}
		return input;
	}

	int getCounter(ZooAnimal a) {
		return a.counter;
	}

	void changeMealsPerDay(ZooAnimal& myAnimal) {
		if (myAnimal.gainedOrLoosedLotOfWeight() == 1) {
			myAnimal.changeMeals(-1);
		}
		else if (myAnimal.gainedOrLoosedLotOfWeight() == -1) {
			myAnimal.changeMeals(1);
		}
	}
}

void Bird::inputAnimalData(istream & input) {
	cout << "Incubation time: ";
	input >> incubation_time;
	cout << "Average body temperature: ";
	input >> average_body_temperature;
	cout << "Prescribed amount of food per meal: ";
	input >> prescribed_amount_of_food;
}
void Mammal::inputAnimalData(istream & input) {
	cout << "Gestational period: ";
	input >> gestational_period;
	cout << "Average body temperature: ";
	input >> average_body_temperature;
	cout << "Prescribed amount of food per meal: ";
	input >> prescribed_amount_of_food;
}

void Reptile::inputAnimalData(istream & input) {
	cout << "Incubation time: ";
	input >> incubation_time;
	cout << "Ambient temperature: ";
	input >> ambient_temperature;
	cout << "Prescribed amount of food per meal: ";
	input >> prescribed_amount_of_food;
}
