#include "Elephant.h"
#include "Mammal.h"
using namespace oss;

Elephant::Elephant(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Mammal("Elephant", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	gestational_period = 8;
	average_body_temperature = 36.3;
	prescribed_amount_of_food = 100;
}
