#include "Tiger.h"
#include "Mammal.h"
using namespace oss;

Tiger::Tiger(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Mammal("Tiger", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	gestational_period = 5;
	average_body_temperature = 30.3;
	prescribed_amount_of_food = 50;
}