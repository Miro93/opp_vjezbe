#include "Mammal.h"
using namespace oss;

class Elephant : public Mammal {
public:
	Elephant(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Elephant() : Mammal() {}
};