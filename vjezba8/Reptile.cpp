#include "Reptile.h"
#include "ZooAnimal.h"
using namespace oss;

void Reptile::printAnimalData(ostream & output) const {
	output << "Reproduction way: " << reproduction_way << endl
		<< "Incubation time: " << incubation_time << " months" << endl
		<< "Ambient temperature: " << ambient_temperature << "\370C" << endl
		<< "Prescribed amount of food per day: " << prescribed_amount_of_food * mealsPerDay << " grams" << endl;
}
