#include "ZooAnimal.h"
#pragma once
using namespace oss;

class Mammal : public ZooAnimal {
	protected:
		string reproduction_way;
		int gestational_period;
		double average_body_temperature;
		int prescribed_amount_of_food;
	public:
		Mammal(string aSpecies, string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
			: ZooAnimal(aSpecies, aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime), reproduction_way("giving birth") {}
		Mammal() : ZooAnimal(), reproduction_way("giving birth"), gestational_period(0), average_body_temperature(0), prescribed_amount_of_food(0) {}
		void printAnimalData(ostream &output) const override;
		void inputAnimalData(istream &input) override;
	};