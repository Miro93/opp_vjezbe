﻿#include "Mammal.h"
#include "ZooAnimal.h"
using namespace oss;

void Mammal::printAnimalData(ostream & output) const {
	output << "Reproduction way: " << reproduction_way << endl
		<< "Gestational period: " << gestational_period << " months" << endl
		<< "Average body temperature: " << average_body_temperature << "\370C" << endl
		<< "Prescribed amount of food per day: " << prescribed_amount_of_food * mealsPerDay << " grams" << endl;
}
