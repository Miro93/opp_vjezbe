#include "Monkey.h"
#include "Mammal.h"
using namespace oss;

Monkey::Monkey(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Mammal("Monkey", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	gestational_period = 3;
	average_body_temperature = 34.3;
	prescribed_amount_of_food = 20;
}