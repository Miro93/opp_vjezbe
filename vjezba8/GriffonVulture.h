#pragma once
#include "Bird.h"
using namespace oss;

class GriffonVulture : public Bird {
public:
	GriffonVulture(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	GriffonVulture() : Bird() {}
};
