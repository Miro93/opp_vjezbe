#include "Crocodile.h"
#include "Reptile.h"


Crocodile::Crocodile(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Reptile("Crocodile", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	incubation_time = 3;
	ambient_temperature = 41.3;
	prescribed_amount_of_food = 40;
}