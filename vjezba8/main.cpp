﻿#include <iostream>
#include <vector>
#include "ZooAnimal.h"
#include "Bird.h"
#include "Mammal.h"
#include "Reptile.h"
#include "Tiger.h"
#include "Monkey.h"
#include "Elephant.h"
#include "GriffonVulture.h"
#include "Owl.h" 
#include "Crocodile.h"
#include "Turtle.h"
using namespace std;


int main() {
	using namespace oss;

	vector <ZooAnimal*> animals = {
		new Tiger("Tig", 2006, 1, 5, 5),
		new Monkey("Monk", 2010, 2, 3, 6),
		new Elephant("Eli", 2000, 3, 6, 60),
		new GriffonVulture("Gri", 2012, 4, 5, 5),
		new Owl("Ow", 20014, 5, 4, 6),
		new Crocodile("Croc", 2011, 6, 5, 10),
		new Turtle("Turt", 2002, 7, 2, 50)
	};

	size_t length = animals.size();
	for (size_t i = 0; i < length; i++) {
		cout << *animals[i];
	}

	for (size_t i = 0; i < length; i++) {
		delete animals[i];
	}
	return 0;
}