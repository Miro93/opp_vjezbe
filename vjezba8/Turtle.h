#include "Reptile.h"
using namespace oss;

class Turtle : public Reptile {
public:
	Turtle(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Turtle() : Reptile() {}
};
