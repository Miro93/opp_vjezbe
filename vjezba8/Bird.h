#include "ZooAnimal.h"
using namespace oss;
#pragma once

class Bird : public ZooAnimal {
protected:
	string reproduction_way;
	int incubation_time;
	double average_body_temperature;
	int prescribed_amount_of_food;
public:
	Bird(string aSpecies, string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
		: ZooAnimal(aSpecies, aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime), reproduction_way("laying eggs") {}
	Bird() : ZooAnimal(), reproduction_way("laying eggs"), incubation_time(0), average_body_temperature(0), prescribed_amount_of_food(0) {}
	void printAnimalData(ostream &output) const override;
	void inputAnimalData(istream &input) override;
};