#pragma once
#include <string>
#include <vector>

namespace oss {
	using namespace std;

	class ZooAnimal {
	protected:
		string species;
		string name;
		int birthYear;
		int cageNumber;
		unsigned short mealsPerDay;
		unsigned short expectedLifeTime;
		float** weightData;
	public:
		ZooAnimal();
		ZooAnimal(const string aSpecies, const string aName, const int aBirthYear, const int aCageNumber, const unsigned short aMealsPerDay, const unsigned short aExpectedLifeTime);
		ZooAnimal(const ZooAnimal& animalToCopy);
		~ZooAnimal();
		int getMealsPerDay();
		void changeMeals(int x);
		void addWeightData(int year, float wData);
		int gainedOrLoosedLotOfWeight() const;
		static int counter;
		static void incrementCounter();
		static void decrementCounter();
		bool operator==(const ZooAnimal& a);
		ZooAnimal operator++();
		ZooAnimal operator++(int);
		void operator=(const ZooAnimal& a);
		virtual void printAnimalData(ostream &output) const {}
		virtual void inputAnimalData(istream &input) {}
		friend ostream &operator<<(ostream &output, const ZooAnimal& a);
		friend istream &operator>>(istream  &input, ZooAnimal& a);
		friend int getCounter(ZooAnimal a);
	};

	void changeMealsPerDay(ZooAnimal& myAnimal);
}