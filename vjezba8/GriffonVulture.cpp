#include "GriffonVulture.h"
#include "Bird.h"
using namespace oss;

GriffonVulture::GriffonVulture(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime)
	: Bird("Griffon Vulture", aName, aBirthYear, aCageNumber, aMealsPerDay, aExpectedLifeTime) {
	incubation_time = 2;
	average_body_temperature = 31.3;
	prescribed_amount_of_food = 10;
}