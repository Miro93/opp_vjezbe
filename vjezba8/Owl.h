#pragma once
#include "Bird.h"
using namespace oss;

class Owl : public Bird {
public:
	Owl(string aName, int aBirthYear, int aCageNumber, unsigned short aMealsPerDay, unsigned short aExpectedLifeTime);
	Owl() : Bird() {}
};