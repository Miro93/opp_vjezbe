#pragma once
#include <iostream>
#include <string>
using namespace std;

class Error
{
public:
	string Error_message;
	virtual void printError_message() = 0;
	void WriteToFile(string fileName, string errMsg);
};

class Error_operand : public Error
{
public:
	Error_operand();
	void printError_message();
};

class Error_operator : public Error
{
public:
	Error_operator();
	void printError_message();
};

class Error_result : public Error
{
public:
	Error_result();
	void printError_message();
};

int Broj();

char UnosOperatora();

int Rezultat(int x, int y, char o);

