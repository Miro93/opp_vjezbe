#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Exceptions.h"
using namespace std;

int main()
{
	vector<int> Rezultati;
	int x, y, r;
	char znak;

	try
	{
		while (true)
		{
			x = Broj();
			znak = UnosOperatora();
			y = Broj();
			r = Rezultat(x, y, znak);
			Rezultati.push_back(r);
			cout << x << " " << znak << " " << y << " = " << r << endl;
			vector<int>::iterator it;
			for (it = Rezultati.begin(); it != Rezultati.end(); ++it)   
			{
				cout << "\n ispis vektora => " << *it << endl;
			}
			cout << "Nastavite (D/N)?: " << endl;
			char c;
			cin >> c;
			if (tolower(c) == 'n')
				break;
		}
	}
	catch(Error& e) 
	{
		cout << ("Zatecena pogreska\n ");
	}
	catch (...)
	{
		cout << ("Nepoznata iznimka\n");
	}
}
