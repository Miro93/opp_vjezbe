#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Exceptions.h"
using namespace std;

void Error::WriteToFile(string fileName, string errMsg)
{
	ofstream myFile;
	myFile.open(fileName, ios::out | ios::app);
	myFile << errMsg;
	myFile.close();
}

Error_operand::Error_operand()
{
	printError_message();
}

void Error_operand::printError_message()
{
	Error_message =" Operand error! Unijeli ste pogresan OPERAND!\n";
	cout << Error_message << endl;
	WriteToFile("error.log", Error_message);
}

Error_operator::Error_operator()
{
	printError_message();
}

void Error_operator::printError_message()
{
	Error_message =" Operator error! Unijeli ste pogresan OPERATOR!\n";
	cout << Error_message << endl;
	WriteToFile("error.log", Error_message);
}

Error_result::Error_result()
{
	printError_message();
}

void Error_result::printError_message()
{
	Error_message = " Operacija error! Unijeli ste pogresan ZNAK!\n";
	cout << Error_message << endl;
	WriteToFile("error.log", Error_message);
}

int Broj()
{
	int n;
	cout << "Unesite cijeli broj: ";
	cin >> n;
	if (cin.fail())
		throw Error_operand();
	else
		return n;
}

char UnosOperatora()
{
	char o;
	cout << "Unesite operator (+,-,*,/): ";
	cin >> o;
	if (o != '+' && o != '-' && o != '*' && o != '/')
		throw Error_operator();
	else
		return o;
}

int Rezultat(int x, int y, char o)
{
	switch (o)
	{
	case '+':
		return x + y;
	case '-':
		return x - y;
	case '*':
		return x * y;
	case '/':
		if (y == 0)
			throw Error_result();
		else
			return x / y;
		break;
	default:
		throw Error_result();
	}
}


