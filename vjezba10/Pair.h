#pragma once
#include <iostream>
using namespace std;
template<typename T1, typename T2>

class Pair
{
	T1 first;
	T2 second;
public:
	Pair(const T1& t1 = 0, const T2& t2 = 0) : first(t1), second(t2) {}
	Pair(const Pair<T1, T2>& other) : first(other.first), second(other.second) {}
	bool operator== (const Pair<T1, T2>& other) const
	{
		return first == other.first && second == other.second;
	}
	bool operator != (const Pair<T1, T2>& other) const
	{
		return !(first == other.first && second == other.second);
	}
	bool operator < (const Pair<T1, T2>& other) const
	{
		return first < other.first;
	}
	bool operator > (const Pair<T1, T2>& other) const
	{
		return first > other.first;
	}

	bool operator <= (const Pair<T1, T2>& other) const
	{
		return first <= other.first;
	}

	bool operator >= (const Pair<T1, T2>& other) const
	{
		return first >= other.first;
	}

	void operator = (const Pair<T1, T2>& other) const
	{
		first = other.first;
		second = other.second;
	}

	friend ostream& operator<< (ostream& o, const Pair<T1, T2>& a)  
	{
		o << a.first << " " << a.second;
		return o;
	}

	friend istream& operator>> (istream& i, const Pair<T1, T2>& a)  
	{
		i >> a.first >> a.second;
		return i;
	}
	void swapp(Pair<T1, T2>& other)   
	{
		T1 temp = first;
		T2 temp2 = second;

		first = other.first;
		second = other.second;

		other.first = temp;
		other.second = temp2;
	}
};

template <>
class Pair<char*, char*>
{
public:
	char first;
	char second;
	Pair(const char& t1 = 'a', const char& t2 = 'b') : first(t1), second(t2) {};
	Pair(const Pair<char*, char*>& other) : first(other.first), second(other.second) {};
	~Pair() {};

	friend ostream& operator<< (ostream& o, const Pair<char*, char*>& c)
	{
		o << c.first << " " << c.second;
		return o;
	}

	friend istream& operator>>(istream& i, Pair<char*, char*>& c)
	{
		i >> c.first >> c.second;
		return i;
	}

	bool operator< (const Pair<char*, char*>& other) const
	{
		return first < other.first;
	}
	bool operator> (const Pair<char*, char*>& other) const
	{
		return first > other.first;
	}
};
