#include <iostream>
#include <vector>
#include <algorithm>
#include "Pair.h"
#include <iterator>
using namespace std;

int main()
{
	Pair<float, int> a(1);
	Pair<float, int> b(9, 4);
	Pair<float, int> c;
	cout << a << endl << b << endl << c << endl;
	cout << "Veci je: " << (a > b) << "\nManji je: " << (a < b) << "\nJednak je: " << (a == b) << "\nRazlicit je: " << (a != b) << endl;
	a.swapp(b);
	cout << "a " << a << "\nb " << b << "\n";

	Pair<char*, char*> p1, p2, p3;
	vector<Pair<char*, char*> > v;
	cin >> p1 >> p2 >> p3;
	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);
	sort(v.begin(), v.end());
	for (vector<Pair<char*, char*> > ::iterator it = v.begin(); it != v.end(); ++it)
	{
		cout << *it << endl;
	}
}
