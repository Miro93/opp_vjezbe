/* 2. Napisite funkciju koja odvaja parne i neparne brojeve u nizu tako da se prvo pojavljuju
parni brojevi, a zatim neparni brojevi. Memoriju za niz alocirati dinamicki. Primjer
ulaza i izlaza:
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27 */

#include <iostream>
using namespace std;

void funk(int niz[], int n)
{
	int i, j = 0;
	int *novi;
	novi = new int[n];

	for (i = 0; i < n; i++)
	{
		if (niz[i] % 2 == 0)
		{
			novi[j++] = niz[i];
		}
	}
	for (i = 0; i < n; i++)
	{
		if (niz[i] % 2 != 0)
		{
			novi[j++] = niz[i];
		}
	}
	for (j = 0; j < n; j++)
	{
		cout << " " << novi[j];
	}
	delete[] novi;
	novi = 0;
}

int main()
{
	int niz[] = { 7,2,4,9,10,11,13,27 }, n;
	n = sizeof(niz) / sizeof(niz[0]);
	funk(niz, n);
}
