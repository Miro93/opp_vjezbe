/* 1. Napisati funkciju koja pronalazi element koji nedostaje u cjelobrojnom rasponu 1-9.
Memoriju za niz alocirati dinamicki, korisnik popunjava niz, a funkcija prepoznaje i
vraca modificirani sortirani niz s brojem koji nedostaje. U main funkciji ispisati dobiveni
niz i osloboditi memoriju. */

#include <iostream>
#include <algorithm>
using namespace std;

int *funk(int niz[], int n)
{
	int i, j = 0;
	int const ukupno = 45;
	int *novi;
	novi = new int[n + 1];
	int suma2 = 0, broj = 0;

	for (i = 0; i < n; i++)
	{
		suma2 = suma2 + niz[i];
	}
	broj = ukupno - suma2;
	novi[j++] = broj;

	for (i = 0; i < n; i++)
	{
		novi[j++] = niz[i];
	}
	sort(novi, novi + n + 1);
	return novi;
}

int main()
{
	int niz[25];
	int n = 8, i, broj, j = 0;

	cout << "unesi brojeve u rasponu od 1-9: \n";
	for (i = 0; i < n; i++)
	{
		cout << "unesi broj =>  ";
		cin >> broj;
		while (broj < 1 || broj > 9)
		{
			cout << "broj NIJE u dozvoljenom rasponu, unesi ponovno => ";
			cin >> broj;
		}
		niz[i] = broj;
	}
	int *povrat = funk(niz, n);
	for (int j = 0; j < n + 1; j++)
	{
		cout << " " << povrat[j];
	}
	delete[] povrat;
	povrat = NULL;
}
