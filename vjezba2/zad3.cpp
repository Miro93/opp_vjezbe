/*3. Defnirati strukturu koja simulira rad vektora.Struktura se sastoji od niza int ele -
menata, logicke i zicke velicine niza.Fizicka velicina je inicijalno init, a kada se ta
velicina napuni vrijednostima, alocira se duplo.
Napisati implementacije za funkcije vector new, vector delete, vector push back,
vector pop back, vector front, vector back i vector size.  */


#include <iostream>
#include <vector>
using namespace std;

struct vektor
{
	int fizicka = 0;
	int logicka = 0;
	int *elementi;

	void vector_new(int velicina_vektora)
	{
		if (velicina_vektora <= 0)
		{
			cout << "Minimalna velicina vektora je 1" << endl;
		}
		else
		{
			elementi = new int[velicina_vektora];
			logicka = velicina_vektora;
		}
	}

	void vector_delete()
	{
		delete[] elementi;
		logicka = 0;
		fizicka = 0;
	}

	void vector_push_back(int x)
	{
		elementi[fizicka] = x;
		fizicka++;
		if (fizicka == (logicka - 1))
		{
			logicka = logicka * 2;
			int * resize_niz = new int[logicka];
			for (int i = 0; i < fizicka; i++)
			{
				resize_niz[i] = elementi[i];
			}
			delete[] elementi;
			elementi = resize_niz;
		}
	}

	void vector_pop_back()
	{
		int *noviNiz = new int[fizicka - 1];
		for (int i = 0; i < fizicka - 1; i++)
		{
			noviNiz[i] = elementi[i];
		}
	}

	int &vector_front()
	{
		return elementi[0];
	}

	int &vector_back()
	{
		return elementi[fizicka - 1];
	}

	int vector_size()
	{
		return fizicka;
	}
};

int main()
{

	int velicina = 5;
	int y;
	vektor mojVektor;
	mojVektor.vector_new(velicina);

	cout << "Unesi br elemenata";
	cin >> y;
	for (int i = 0; i < y; i++)
	{
		int x;
		cin >> x;
		mojVektor.vector_push_back(x);
	}
	cout << "push_back" << endl;

	for (int i = 0; i < mojVektor.fizicka; i++)
		cout << mojVektor.elementi[i];
	cout << endl;

	mojVektor.vector_pop_back();
	cout << "pop_back" << endl;

	for (int i = 0; i < mojVektor.fizicka - 1; i++)
	{
		cout << mojVektor.elementi[i];
	}
	cout << endl;
	cout << "vector_back" << endl;
	cout << mojVektor.vector_back() << endl;
	cout << "vector_front" << endl;
	cout << mojVektor.vector_front() << endl;
	cout << "vector_size" << endl;
	cout << mojVektor.vector_size() << endl;
	mojVektor.vector_delete();
	for (int i = 0; i < mojVektor.fizicka; i++)
		cout << mojVektor.elementi[i];
	cout << endl;
}