#pragma once
#include <iostream>
using namespace std;

namespace oop 
{
	class Array 
	{
	private:
		int* arr;
		int n;
		static int counter;
	public:
		Array(int n);
		Array(const Array& a);
		~Array();
		static void clan();
		Array operator+(const Array& a);
		Array operator-(const Array& a);
		Array operator=(const Array& a);
		bool operator==(const Array& a);
		bool operator!=(const Array& a);
		int & operator[](int index);
		friend std::ostream &operator<<(std::ostream &output, const Array &a);
		friend std::istream &operator>>(std::istream &input, const Array &a);
	};
}