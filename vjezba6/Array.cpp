#include "Array.h"
using namespace std;
using namespace oop;
int Array::counter = 0;

Array::Array(int n)
{
	this->n = n;
	arr = new int[n];
	counter++;
}

Array::Array(const Array & a)
{
	arr = new int[a.n];
	n = a.n;
	for (size_t i = 0; i < n; i++)
	{
		arr[i] = a.arr[i];
	}
}

Array Array::operator+(const Array & a)
{
	Array tempArr(n + a.n);
	for (size_t i = 0; i < n ; i++)
	{
		tempArr.arr[i] = arr[i];
	}
	for (size_t i = 0; i < a.n; i++)
	{
		tempArr.arr[n + i] = a.arr[i];
	}
	return tempArr;
}

Array Array::operator-(const Array & a)
{
	Array tempArr(n);
	for (size_t i = 0; i < n; i++)
	{
		for (size_t i = 0; i < a.n; i++)
		{
			//cout << "\na.arr-> " << a.arr[i];
			//cout << "\narr-> " << arr[i];
			if (arr[i] != a.arr[i])
			{
				tempArr[i] = arr[i];
				//cout << " temp->" << tempArr[i];
			}
		}	
	}
	return tempArr;
}

Array Array::operator=(const Array & a)
{
	return Array(a);
}

bool Array::operator==(const Array & a)
{
	if (this == &a)
		return true;
	for (size_t i = 0; i < n; ++i) {
		if (arr[i] != a.arr[i])
			return false;
	}
	return true;
}

bool Array::operator!=(const Array & a)
{
	if (this == &a)
		return false;
	for (size_t i = 0; i < n; ++i) {
		if (arr[i] != a.arr[i])
			return true;
	}
	return false;
}

int &Array::operator[](int index)
{
	if (index >= n)
	{
		cout << "Array index out of bound, exiting";
		exit(0);
	}
	return arr[index];
}

std::ostream & oop::operator<<(std::ostream & output, const Array & a)
{
	for (size_t i = 0; i < a.n; i++)
	{
		output << a.arr[i] << " ";
	}
	return output;
}

std::istream & oop::operator>>(std::istream & input, const Array & a)
{
	for (size_t i = 0; i < a.n; i++)
	{
		input >> a.arr[i];
	}
	return input;
}

void Array::clan()
{
	cout << "\nBrojac-> " << counter << endl;
}

Array::~Array()
{
	delete[] arr;
	arr = nullptr;
	counter--;
}
