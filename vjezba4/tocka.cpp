#include <math.h>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include "tocka.h"
using namespace std;


void pozicijaTocke::postavi_vrijed(double a, double b, double c)
{
	duzina = a;
	sirina = b;
	visina = c;
}

void pozicijaTocke::pseudorandom_vrijed(int a, int b)
{
	if (a > b)
	{
		int temp = a;
		a = b;
		b = temp;
	}
	srand(time(NULL));
	duzina = rand() % (b - a + 1) + a;
	sirina = rand() % (b - a + 1) + a;
	visina = rand() % (b - a + 1) + a;
}

double pozicijaTocke::dohvati_duzina()
{
	return duzina;
}

double pozicijaTocke::dohvati_sirina()
{
	return sirina;
}

double pozicijaTocke::dohvati_visina()
{
	return visina;
}

double pozicijaTocke::udaljenost2D(pozicijaTocke p)
{
	return sqrt(pow(duzina - p.duzina, 2) + pow(sirina - p.sirina, 2));
}

double pozicijaTocke::udaljenost3D(pozicijaTocke p)
{
	return sqrt(pow(duzina - p.duzina, 2) + pow(sirina - p.sirina, 2) + pow(visina - p.visina, 2));
}








