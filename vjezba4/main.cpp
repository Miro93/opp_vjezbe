#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "tocka.h"
#include "oruzje.h"
#include "meta.h"

using namespace std;

vector<Meta> generirajMete(int n) {
	vector<Meta> mete;
	for (int i = 0; i < n; i++) {
		Meta m = Meta();
		pozicijaTocke p = pozicijaTocke();
		double x, y, z;
		cout << "\nUnesi x, y, z : " << endl;
		cin >> x >> y >> z;
		p.postavi_vrijed(x, y, z);
		m.donjaLijeva = p;
		mete.push_back(m);
	}
	return mete;
}


int main()
{
	pozicijaTocke tocka1, tocka2;

	cout << "\nUnesena vrijednost clanova: ";
	tocka2.postavi_vrijed(10, 11, 12);
	cout << "\nDuzina je -> " << tocka2.dohvati_duzina();
	cout << "\nsirina je -> " << tocka2.dohvati_sirina();
	cout << "\nvisina je -> " << tocka2.dohvati_visina();
	cout << "\n";


	cout << "\nRandom vrijednost clanova: ";
	tocka1.pseudorandom_vrijed(1, 100);
	cout << "\nDuzina je -> " << tocka1.dohvati_duzina();
	cout << "\nsirina je -> " << tocka1.dohvati_sirina();
	cout << "\nvisina je -> " << tocka1.dohvati_visina();
	cout << "\n";

	cout << "\n2D & 3D udaljenost: ";
	double distance2d = tocka1.udaljenost2D(tocka2);
	cout << "\nUdaljenost do druge pozicije-> " << distance2d;
	double distance3d = tocka1.udaljenost3D(tocka2);
	cout << "\nUdaljenost do druge pozicije-> " << distance3d;
	cout << "\n";

	oruzje pistolj;
	pozicijaTocke p;
	double x, y, z;
	cout << "Unesi poziciju oruzja x, y, z : " << endl;
	cin >> x >> y >> z;
	p.postavi_vrijed(x, y, z);
	pistolj.polozaj = p;

	int n;
	cout << "Unesi broj meta " << endl;
	cin >> n;
	vector<Meta> mete = generirajMete(n);

	while (pistolj.brojMetaka != 0) 
	{
		pistolj.shoot();
	}
	for (int i = 0; i < mete.size(); i++)
	{
		if (((pistolj.polozaj.visina) > (mete[i].donjaLijeva.visina)) && (pistolj.polozaj.visina < mete[i].donjaLijeva.visina + mete[i].visina))
		{
			mete[i].stanje = true;
			cout << "\nMeta je POGODJENA!";
		}
		else
			cout << "\nMeta je PROMAsENA!";
	}
	cout << "\nBroj metaka nakon pucanja -> " << pistolj.brojMetaka;
	pistolj.reload();
	cout << "\nBroj metaka nakon punjenja -> " << pistolj.brojMetaka;
	



	
}
