#pragma once
using namespace std;
#include <iostream>

class pozicijaTocke
{

public:

	double duzina;
	double sirina;
	double visina;

	void postavi_vrijed(double a, double b, double c);

	void pseudorandom_vrijed(int a, int b);

	double dohvati_duzina();

	double dohvati_sirina();
	
	double dohvati_visina();

	double udaljenost2D(pozicijaTocke p);
	
	double udaljenost3D(pozicijaTocke p);

};