#include "oruzje.h"

oruzje::oruzje()
{
	brojMetaka = 6;
}

void oruzje::shoot()
{
	brojMetaka--;
}

void oruzje::reload()
{
	brojMetaka = 6;
}

oruzje::~oruzje()
{
}