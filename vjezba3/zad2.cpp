/*2. Koristeci funkcije iz prvog zadatka unijeti dva vektora i formirati novi vektor koji se sas-
toji od elemenata iz prvog vektora koji nisu u drugom vektoru. Koristiti binary search
funkciju. */

#include<iostream>
#include <algorithm>
#include <vector>
#include <time.h> 
#include <stdlib.h> 
#include "zad1.h"
using namespace std;

vector<int> formiranje(vector<int> mojVektor, bool izbor, const int a, const int b, int n)
{
	int broj;
	vector<int> mojVektor1;
	vector<int> mojVektor2;
	srand((unsigned int)time(NULL));
	if (izbor == true)
	{
		for (int i = 0; i < n; i++)
		{
			cout << "unesi brojeve => ";
			cin >> broj;
			while (broj <= a || broj > b)
			{
				cout << "broj je izvan GRANICE, unesi ponovno => ";
				cin >> broj;
			}
			mojVektor1.push_back(broj);
		}
		cout << "\n";
		for (int i = 0; i < n; i++)
		{
			cout << "unesi brojeve => ";
			cin >> broj;
			while (broj <= a || broj > b)
			{
				cout << "broj je izvan GRANICE, unesi ponovno => ";
				cin >> broj;
			}
			mojVektor2.push_back(broj);
		}

	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			mojVektor2.push_back(rand() % 100);
		}
		cout << "\n";
		for (int i = 0; i < n; i++)
		{
			mojVektor1.push_back(rand() % 100);
		}
	}

	sort(mojVektor1.begin(), mojVektor1.end());
	sort(mojVektor2.begin(), mojVektor2.end());
	vector<int> vektorNOVI;
	vector<int> ::iterator it;
	for (it = mojVektor1.begin(); it != mojVektor1.end(); ++it)
	{
		if (!binary_search(mojVektor2.begin(), mojVektor2.end(), *it))
		{
			vektorNOVI.push_back(*it);
		}
	}
	return vektorNOVI;
}


void ispis(vector<int> mojVektor, bool izbor, const int a, const int b, int n)
{
	vector<int>povrat = formiranje(mojVektor, izbor, a, b, n);
	for (int i = 0; i < n; i++)
	{
		cout << povrat[i] << endl;
	}
}


