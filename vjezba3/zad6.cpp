/*6. Korisnik unosi prirodni broj N, nakon cega unosi N stringova u vector (duljina svakog
stringa mora biti manja od 20 - provjeriti unos, dozvoljeni su samo brojevi i velika slova
engleske abecede). Ukoliko string sadrzava slova i brojeve, onda ga treba trasnformirati
na nacin da se svaki podstring od 2 ili vise istih znakova zamijeni brojem ponavlja-
nja (AAA = 3A). S druge strane, ukoliko string sadrzava samo slova onda ga treba
transformirati suprotno prethodno opisanoj transformaciji (2F = FF).
npr. N=5,
Unos: AABFBBBBCC, AABFBBBCCC AAAAFBFFFBBBCCC, 2F8H4SF
Ispis: 2ABF4B2C, 2ABF3B3C, 4AFB3F3B3C, FFHHHHHHHHSSSSF*/


#include<iostream>
#include <vector>
#include <time.h> 
#include <stdlib.h> 
#include <string>
using namespace std;


bool provjera(string& s)
{
	std::string::iterator it = s.begin();
	while (it != s.end() && (isdigit(*it) || isupper(*it))) ++it;
	return !s.empty() && it == s.end();
}

void transformacija1(vector<string> vektor, string& s)
{
	string novi;
	int broj;
	int br = 1;
	for (int i = 0; i < vektor.size(); ++i)
	{
		for (int j = 0; j < vektor[i].size(); j++)
		{
			for (int k = j + 1; k < vektor[i].size() + 1; k++)
			{
				if (isdigit(s[j]) && isalpha(s[k]))
				{
					broj = (s[j] - '0');
					while (broj != 0)
					{
						novi.push_back(s[k]);
						broj--;
					}
					j++;
				}
				else
				{
					if (isalpha(s[k]))
					{
						novi.push_back(s[k]);
					}
					j = k;
					k = j;
				}
			}
		}
	}
	cout << "\ => " << novi;
}

void transformacija2(vector<string> vektor, string& s)
{
	string novi;

	int br = 1;
	for (int i = 0; i < vektor.size(); ++i)
	{
		for (int j = 0; j < vektor[i].size(); j++)
		{
			for (int k = j + 1; k < vektor[i].size() + 1; k++)
			{
				if (s[j] == s[k])
				{
					br++;
					j++;
					if (br == 2)
					{
						novi.push_back(s[j]);
					}
					else
						continue;
				}
				else
				{
					novi += std::to_string(br);
					novi.push_back(s[k]);
					br = 1;
					j = k + 1;
					k = j;
				}
			}
		}
	}
	cout << "\ => " << novi;
}


int main()
{
	vector<string> vektor;
	string s;
	int N = 0;

	cout << "koliko stringova zelis? => ";
	cin >> N;

	if (N <= 0)
	{
		cout << "Broj mora bit veci od 0!";
		return 0;
	}

	cin.ignore(1, '\n');
	cout << " string se smije sastojat samo od VELIKIH/BROJEVA!\n";
	for (int i = 1; i < N + 1; i++)
	{
		cout << "Unesi " << i << ". string: ";
		getline(cin, s);

		while (!(provjera(s) == true) || (s.length() > 20))
		{
			cout << "string je prevelika il je kriv unos, unesi ponovno => ";
			getline(cin, s);
		}
		vektor.push_back(s);
	}


	for (int i = 0; i < vektor.size(); ++i)
	{
		if (isalpha(s[i]) && isdigit(s[i]))
		{
			transformacija1(vektor, s);  //brojevi i slova!
		}
		if (isalpha(s[i]))
		{
			transformacija2(vektor, s);  //samo slova!
		}
	}
}