/*5. Napisati strukturu "Producent" s atributima "name", "movie" i "year". Korisnik upi-
suje podatke u vektor struktura Producent. Napisi funkciju koja pronalazi najzastup-
ljenijeg producenta (moguce je vise najzastupljenijih). */

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <iterator>
#include <map>
using namespace std;


struct producent
{
	string name;
	string movie;
	int year;
};


void printanje(vector<producent> prod, int N)
{
	for (int i = 0; i < N; i++)
	{
		cout << "\nIME: " << prod[i].name << "\nFILM: " << prod[i].movie << "\nGODINA: " << prod[i].year << endl;
	}
	cout << endl;
}

void najzastupljenij(vector <string> novi)
{
	map<string, int> podaci;
	for (auto i : novi)
		++podaci[i];

	std::multimap<int, string, std::greater<int> > dst;

	transform(podaci.begin(), podaci.end(), std::inserter(dst, dst.begin()), [](const std::pair<std::string, int> &p)
	{
		return std::pair<int, std::string>(p.second, p.first);
	}
	);
	std::multimap<int, std::string>::iterator t = dst.begin();

	cout << "\nnajzastupljeniji producenti su: \n";
	for (int count = 0; count < 3 && t != dst.end(); ++t, ++count)
		std::cout << t->second << "-> " << t->first << std::endl;
}


int main()
{
	vector <producent> prod;
	//prod.push_back({ "miro","lotr",1216, });
	int N;
	cout << "unesi broj producenata=> ";
	cin >> N;
	prod.resize(N);

	for (int i = 0; i < N; i++)
	{
		cout << "\nUpisi podatke o " << i + 1 << ". Producentu";
		prod.push_back(producent());
		cout << "\n\tUpisi ime : ";
		cin >> prod[i].name;

		cout << "\n\tUpisi naziv filma : ";
		cin >> prod[i].movie;

		cout << "\n\tUpisi godinu filma: ";
		cin >> prod[i].year;
	}
	printanje(prod, N);
	vector <string> novi;
	for (auto vectorit = prod.begin(); vectorit != prod.end(); ++vectorit)
	{
		cout << "\n" << (*vectorit).name;     // printanje SAMO imena !
		novi.push_back((*vectorit).name);
	}

	vector<string>::iterator it;
	for (it = novi.begin(); it != novi.end(); ++it)   //provjera
	{
		cout << "=> " << *it << endl;
	}
	najzastupljenij(novi);
}






