﻿/* 4. Koriste´ci vector napravite implementaciju igre ˇsibica gdje korisnik igra protiv raˇcunala.
Pravila ove igre su vrlo jednostavna. Pred 2 igraˇca postavi se 21 ˇsibica. Igraˇci se
izmjenjuju i uklanjaju 1, 2 ili 3 ˇsibice odjednom. Igraˇc koji je prisiljen uzeti posljednju
ˇsibicu gubi.
Korisnik unosi izbor, dok se za odabir raˇcunala bira sluˇcajnim izborom. Igraˇcu se mora
dati prednost, tako da raˇcunalo prvo zapoˇcinje igru.
Prije pisanja koda promislite o problemu, koji su mogu´ci sluˇcajevi,
 kad je sigurna pobjeda, kad je neizbjeˇzan poraz (proˇcitati ”Problem ˇsibica - pomo´c”). */


#include<iostream>
using namespace std;

int sibice = 21, n = 0;
void ostatak_sibica()
{
	sibice = sibice - n;
	cout << "\nOstalo je = " << sibice << " sibica";
	if (sibice == 1)
	{
		cout << "\nTi gubis!";
	}
}
void cpu_odabir()
{
	if (n == 1)
	{
		sibice = sibice - 3; cout << "\nCPU uzima 3"; n = 0;
	}
	else if (n == 2)
	{
		sibice = sibice - 2; cout << "\nCPU uzima 2"; n = 0;
	}
	else if (n == 3)
	{
		sibice = sibice - 1; cout << "\nCPU uzima 1"; n = 0;
	}

}
int main()
{
	cout << "Imamo 21 sibicu";
	do
	{
		cout << "\nIzaberi sibice => ";
		cin >> n;
		if (n != 1 && n != 2 && n != 3)
		{
			cout << "Pogresan unos!"; n = 0; continue;
		}
		ostatak_sibica();
		cpu_odabir();
		ostatak_sibica();
	} while (sibice != 1);
	return 0;
}


//PROBLEM SIBICA, POMOC
/*Pravila
Pravila ove igre su vrlo jednostavna. Za poˇcetak, ispred dva igraˇca nalazi se 21 ˇsibica.
Igraˇci se izmjenjuju i uklanjaju 1, 2 ili 3 ˇsibice odjednom. Igraˇc koji je prisiljen uzeti posljednju
ˇsibicu je izgubio.
Pojednostavite problem
U programiranju, obiˇcno je loˇsa ideja poˇceti pisati kod prije nego ˇsto budemo imali ideju o
tome ˇsto pokuˇsavamo posti´ci. Ne moramo nuˇzno imati na umu detaljnu strukturu ili dizajn da
bismo zapoˇceli kodiranje, ali trebamo u glavi imati nekakav grubi oblik rjeˇsenja. Za poˇcetak,
pojednostavimo ovaj problem onoliko koliko moˇzemo, a zatim od tamo napredujemo. Koji je
najjednostavniji mogu´ci sluˇcaj?
Recimo da je naˇs red i da je preostala joˇs samo 1 ˇsibica. Prisiljeni smo je uzeti, i zato
gubimo.
Dalje, ˇsto ako postoje 2 ˇsibice? U tom sluˇcaju moˇzemo uzeti 1, i prisiliti drugog igraˇca
da uzme posljednju ˇsibicu. Dakle, ako postoje 2 ˇsibice, a naˇs je red, pobjedujemo!
3 ˇsibice? Uzmemo 2, ostavljamo 1, i joˇs uvijek pobjedujemo.
Sto je sa 4? Uzmemo 3 ˇsibice, ostavivˇsi posljednju, ponovno pobje ˇ dujemo!
Sto se doga ˇ da kada dodemo do 5 ˇsibica? Ovdje smo u nevolji. Bez obzira ˇsto da napravimo,
naˇs protivnik ´ce zavrˇsiti s 2, 3 ili 4 ˇsibice. I znamo da od te toˇcke moˇze pobijediti (ako razmiˇslja
normalno).
Sablona ˇ
Sada moˇzemo vidjeti induktivni uzorak: sa samo 1 ˇsibicom, gubimo. Moˇzemo pobijediti
za brojanje 2, 3 i 4. Izgubili smo joˇs jednom na 5. Sada se isti uzorak ponavlja. Ako je
preostalo 6, 7 ili 8 ˇsibica, moˇzemo dovesti protivnika u gubitniˇcku poziciju ostavljaju´ci 5
ˇsibica. U sluˇcaju 9 ˇsibica, opet smo u gubitniˇckoj poziciji i tako dalje.
To moˇzemo saˇzeti formulom: Ako postoje m * 4 + 1 ˇsibice, onda smo u gubitniˇckoj
poziciji. Ovdje je m cijeli broj izmedu 0 i 5, a 4 je jedan viˇse od maksimalnog broja ˇsibica
koje igraˇc moˇze uzeti u isto vrijeme. Naˇs cilj je stoga uvijek dovesti protivnika do sljede´ceg
dostupnog od tih gubitniˇckih pozicija: 17, 13, 9, 5, 1.
Moˇzemo vidjeti da je igraˇc koji zapoˇcne igru naˇzalost osuden na poraz, osim ako drugi
igraˇc napravi pogreˇsku kod odabira broja ˇsibica. */