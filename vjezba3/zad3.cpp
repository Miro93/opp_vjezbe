/*3. Ucitati string koji predstavlja recenicu. Napisati funkciju koja iz stringa izbacuje sve
praznine koje se nalaze ispred znakova interpunkcije i dodaje praznine nakon znaka
interpunkcije ako nedostaju.
Primjer: Za recenicu "Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .",
ispravna recenica glasi: "Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.". */

#include<iostream>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;

void funk(string &recenica)
{
	while (true)
	{
		auto pos = recenica.find(" .");
		if (pos == std::string::npos)
			break;
		recenica.replace(pos, 2, ".");
	}
	while (true)
	{
		auto pos = recenica.find(" ,");
		if (pos == std::string::npos)
			break;
		recenica.replace(pos, 2, ",");
	}
	string novi;
	for (int i = 0; i < recenica.size(); i++)
	{
		for (int j = i + 1; j < recenica.size() + 1; j++)
		{
			novi.push_back(recenica[i]);
			if ((recenica[i] == ',') && (isalpha(recenica[j])))
			{
				novi.push_back(' ');
				i++;
			}
			else
				i++;
		}
	}
	cout << "\n  novi-> " << novi;
}
int main()
{
	char cstr[] = "Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .";
	string recenica(cstr);
	funk(recenica);
}