
// 3. U nizu cetveroznamenkastih brojeva nalazi se samo jedan broj cija je suma jedinice i
// stotice jednaka 5. Napisite funkciju koja pronalazi taj broj u nizu od N brojeva i vraca
// referencu na taj element niza.Koristeci povratnu vrijednost funkcije kao lvalue uvecajte
// taj element niza za jedan.Ispisite vrijednost elementa poslije uvecavanja.

#include <iostream>
using namespace std;

int &funk(int niz[], int n, int *index)
{
	int i;
	for (i = 0; i < n; i++)
	{
		if (((niz[i] % 10) + ((niz[i] / 100) % 10)) == 5)
		{
			*index = i;
			return niz[i];
		}
	}
}
int main()
{
	int i, n, broj;
	int niz[100];
	cout << "Unesi velicinu niza=> ";
	cin >> n;
	for (i = 0; i < n; i++)
	{
		cout << "unesi cetveroznamenkasti broj => ";
		cin >> broj;
		while (broj < 1000 || broj > 9999)
		{
			cout << "broj nije CETVEROZNAMENKAST, unesi ponovno => ";
			cin >> broj;
		}
		niz[i] = broj;
	}
	int index = 0;
	funk(niz, n, &index) += 1;

	cout << "Element poslije uvecanja=> " << niz[index];

}
