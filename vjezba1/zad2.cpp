/*2. Napisite program za vodenje evidencije i statisticku analizu za razred od 20 studenata.
Informacije o svakom studentu sadrze ID string, ime, spol, ocjene kvizova(2 kviza
	po semestru), ocjenu na sredini semestra(mid - term score), ocjenu na kraju semestra
	(nal score) i ukupan broj bodova koji je zbroj kviza1, kviza2, ocjene na sredini i ocjene
	na kraju semestra(total score).Kreiraj strukturu student i napisi implementaciju za
	izbornik ispod
	1-> Dodaj novi zapis								DONE!
	2-> Ukloni zapis									DONE!
	3-> Azuriraj zapis									DONE!
	4-> Prikazi sve zapise								DONE!
	5-> Izracunaj prosjek bodova za studenta			DONE!
	6-> Prikazi studenta s najvecim brojem bodova		DONE!
	7-> Prikazi studenta s najmanjim brojem bodova		DONE!
	8-> Pronadi studenta po ID - u                      DONE!
	9-> Sortiraj zapise po broju bodova(total)			DONE!
	10-> Izlaz										    DONE!

	Imati na umu da se korisniku ne smije dozvoliti unos za studenta s vec postojecim ID -		DONE!
	ijem, te slucajeve kad u nizu nema zapisa, kad korisnik upise ID koji ne postoji u nizu		DONE!
	i slicno.
*/

#include <iostream>
#include <algorithm>  
#include <string>
using namespace std;


struct Student
{
	string Id;
	char Ime[25];
	char Spol;

	int kviz1;
	int kviz2;
	int Ocijena_sredina;
	int Ocijena_kraj;

	int Ocijena_ukupno;
};

void ukupno(Student& std)
{
	std.Ocijena_ukupno = std.kviz1 + std.kviz2 + std.Ocijena_sredina + std.Ocijena_kraj;
}


void kvizovi_ukupno(Student std[], int n)
{
	for (int i = 0; i < n; i++)
	{
		ukupno(std[i]);
	}
}


void dodaj_novi(Student std[], int *i, int *n)
{
	cout << "\nUpisi podatke o " << *i + 1 << ". Studentu";

	string id;
	cout << "\nUnesi ID=> ";
	cin >> id;
	int flag = 0;
	while (1)
	{
		for (int i = 0; i < *n; i++)
		{
			if (std[i].Id.compare(id) == 0)
			{
				flag++;
				cout << "\nTaj ID se vec KORISTI, pokusaj ponovno => ";
				cin >> id;
			}
		}
		if (flag == 0)
		{
			cout << "\n vas unos je ISPRAVAN, sad upisite podatke o novom studentu: ";
			cout << "\n\tUpisi Id : ";
			cin >> std[*i].Id;

			cout << "\n\tUpisi ime : ";
			cin >> std[*i].Ime;

			cout << "\n\tUpisi spol (m/z) : ";
			cin >> std[*i].Spol;

			cout << "\n\tUpisi ocjenu 1. kviza : ";
			cin >> std[*i].kviz1;

			cout << "\n\tUpisi ocjenu 2. kviza : ";
			cin >> std[*i].kviz2;

			cout << "\n\tUpisi ocjenu na sredini semestra: ";
			cin >> std[*i].Ocijena_sredina;

			cout << "\n\tUpisi ocjenu na kraju semestra : ";
			cin >> std[*i].Ocijena_kraj;

			*i += 1;
			*n += 1;
			break;
		}
		else
			flag = 0;
	}
}


void ukloni(Student std[], int *n)
{
	string id;
	cout << "Unesi ID studenta kojeg zelite UKLONUTI=> ";
	cin >> id;

	for (int i = 0; i < *n; i++)
	{
		if (std[i].Id.compare(id) == 0)
		{
			for (int j = 0; j < (*n - i); j++)
			{
				std[i] = std[i + 1];
				i++;
			}
		}
	}
	*n = *n - 1;

}

void azuriraj(Student std[], int *n)
{
	string id;
	cout << "Unesi ID=> ";
	cin >> id;
	for (int i = 0; i < *n; i++)
	{
		if (std[i].Id.compare(id) == 0)
		{
			cout << "\n";
			cout << "\nUpisi podatke o " << i + 1 << ". Studentu";

			cout << "\n\tUpisi Id : ";
			cin >> std[i].Id;

			cout << "\n\tUpisi ime : ";
			cin >> std[i].Ime;

			cout << "\n\tUpisi spol (m/z) : ";
			cin >> std[i].Spol;

			cout << "\n\tUpisi ocjenu 1. kviza : ";
			cin >> std[i].kviz1;

			cout << "\n\tUpisi ocjenu 2. kviza : ";
			cin >> std[i].kviz2;

			cout << "\n\tUpisi ocjenu na sredini semestra: ";
			cin >> std[i].Ocijena_sredina;

			cout << "\n\tUpisi ocjenu na kraju semestra : ";
			cin >> std[i].Ocijena_kraj;
			break;
		}
	}
}


void prikazi(Student std[], int i)
{
	cout << "\nInformacije o studentima: \n";
	for (int j = 0; j < i; j++)
	{
		cout << "\n" << std[j].Id << "\t" << std[j].Ime << "\t" << std[j].Spol
			<< "\t" << std[j].kviz1 << "\t" << std[j].kviz2 << "\t"
			<< std[j].Ocijena_sredina << "\t" << std[j].Ocijena_kraj << "\t" << std[j].Ocijena_ukupno << "\n";
	}
	cout << "\n \n";
}

/

void najveci_bodovi(Student std[], int *n)
{
	int temp, i;
	int index_max = 0;
	int max;

	max = std[0].kviz1 + std[0].kviz2 + std[0].Ocijena_sredina + std[0].Ocijena_kraj;
	for (i = 1; i < *n; i++)
	{
		temp = (std[i].kviz1 + std[i].kviz2 + std[i].Ocijena_sredina + std[i].Ocijena_kraj);
		if (temp > max)
		{
			max = temp;
			index_max = i;
		}
	}
	cout << "\n NajVECI broj bodova iznosi=> " << max << ",a to je student po rednim brojem  " << index_max + 1 << ". \n";
}


void najmanjim_bodovi(Student std[], int *n)
{
	int temp, i;
	int index_min = 0;
	int min;

	min = std[0].kviz1 + std[0].kviz2 + std[0].Ocijena_sredina + std[0].Ocijena_kraj;
	for (i = 1; i < *n; i++)
	{
		temp = (std[i].kviz1 + std[i].kviz2 + std[i].Ocijena_sredina + std[i].Ocijena_kraj);
		if (temp < min)
		{
			min = temp;
			index_min = i;
		}
	}
	cout << "\n NajMANJI broj bodova iznosi=> " << min << ",a to je student po rednim brojem  " << index_min + 1 << ". \n";
}


void search_id(Student std[], int n)
{
	string id;
	cout << "Unesi ID=> ";
	cin >> id;
	int flag = 0;
	while (1)
	{
		for (int i = 0; i < n; i++)
		{
			if (std[i].Id.compare(id) == 0)
			{
				flag++;
				cout << "\n" << std[i].Id << "\t" << std[i].Ime << "\t" << std[i].Spol;
				cout << "\n";
				break;
			}
		}
		if (flag == 1)
		{
			break;
		}
		else
		{
			cout << "\n Taj index NE POSTOJI, pokusaj ponovno => ";
			cin >> id;
		}
	}
}

bool student_sorter(Student const& a, Student const& b) {
	return a.Ocijena_ukupno < b.Ocijena_ukupno;
}


void sortiranje(Student std[], int n)
{
	kvizovi_ukupno(std, n);
	sort(std, std + n, student_sorter);
	cout << "Sorted Array looks like this." << endl;
	prikazi(std, n);

}

int main()
{
	int i, n;
	Student std[20];
	cout << "unesi broj studenata=> ";
	cin >> n;

	for (i = 0; i < n; i++)
	{
		cout << "\nUpisi podatke o " << i + 1 << ". Studentu";

		cout << "\n\tUpisi Id : ";
		cin >> std[i].Id;

		cout << "\n\tUpisi ime : ";
		cin >> std[i].Ime;

		cout << "\n\tUpisi spol (m/z) : ";
		cin >> std[i].Spol;

		cout << "\n\tUpisi ocjenu 1. kviza : ";
		cin >> std[i].kviz1;

		cout << "\n\tUpisi ocjenu 2. kviza : ";
		cin >> std[i].kviz2;

		cout << "\n\tUpisi ocjenu na sredini semestra : ";
		cin >> std[i].Ocijena_sredina;

		cout << "\n\tUpisi ocjenu na kraju semestra : ";
		cin >> std[i].Ocijena_kraj;
	}


	cout << "\nInformacije o studentima: \n";
	for (i = 0; i < n; i++)
	{
		cout << "\n" << std[i].Id << "\t" << std[i].Ime << "\t" << std[i].Spol
			<< "\t" << std[i].kviz1 << "\t" << std[i].kviz2 << "\t"
			<< std[i].Ocijena_sredina << "\t" << std[i].Ocijena_kraj << "\n";
	}
	cout << "\n \n";


	int izbornik;

	while (1)
	{
		cout << "1. Dodaj novi zapis\n";
		cout << "2. Ukloni zapis\n";
		cout << "3. Azuriraj zapis\n";
		cout << "4. Prikazi sve zapise\n";
		cout << "5. Izracunaj prosjek bodova za studenta\n";
		cout << "6. Prikazi studenta s NAJVECIM brojem bodova\n";
		cout << "7. Prikazi studenta s NAJMANJIM brojem bodova\n";
		cout << "8. Pronadi studenete po ID-u\n";
		cout << "9. Sortiraj zapise po bodovima\n";
		cout << "10. izlaz" << endl;

		cin >> izbornik;
		switch (izbornik)
		{
		case 1:
			dodaj_novi(std, &i, &n);
			cout << "\n";
			break;
		case 2:
			ukloni(std, &n);
			cout << "\n";
			break;
		case 3:
			azuriraj(std, &n);
			cout << "\n";
			break;
		case 4:
			kvizovi_ukupno(std, n);
			prikazi(std, n);
			cout << "\n";
			break;
		case 5:
			//kvizovi_ukupno(std, n);
			//prosjek_bodova(std, &n);
			cout << "\n";
			break;
		case 6:
			najveci_bodovi(std, &n);
			cout << "\n";
			break;
		case 7:
			najmanjim_bodovi(std, &n);
			cout << "\n";
			break;
		case 8:
			search_id(std, n);
			cout << "\n";
			break;
		case 9:
			sortiranje(std, n);
			cout << "\n";
			break;
		case 10:
			return 0;
		default: cout << "Nisi upisao ni jedan od ponudenih brojeva!!" << endl;
			cout << endl;
		}
	}
}

