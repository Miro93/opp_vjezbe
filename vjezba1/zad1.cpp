//1. U nizu od N cijelih brojeva, nalaze se duplikati. Pronadi ih i ispisi koliko puta se
//ponavljaju! Moguce su samo vrijednosti 1-9

#include <iostream>
using namespace std;


int main()
{
	int niz[25], dup[25];
	int n, i, j, br;
	cout << "unesi velicinu niza=> ";
	cin >> n;

	cout << "unesi elemente niza: \n";
	for (i = 0; i < n; i++)
	{
		cin >> niz[i];
		dup[i] = -1;
	}
	for (i = 0; i < n; i++)
	{
		br = 1;
		for (j = i + 1; j < n; j++)
		{
			if (niz[i] == niz[j])
			{
				br++;
				dup[j] = 0;
			}
		}
		if (dup[i] != 0)
		{
			dup[i] = br;
		}
	}
	cout << "\n" << "dupli elementi su: \n";
	for (i = 0; i < n; i++)
	{
		if (dup[i] > 1)
		{
			cout << niz[i] << " " << "a broj ponavljanja je => " << dup[i] << endl;
		}
	}
}